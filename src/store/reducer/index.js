import { combineReducers } from "redux";
import home from "../../main/home/store/reducer/homeReducer";
const createReducer = (asyncReducers) =>
  combineReducers({
    home,
  });

export default createReducer;
