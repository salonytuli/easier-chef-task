import { connect } from "react-redux";
import Home from "./main/home/home";

function App() {
  return <Home />;
}

export default connect(null, null)(App);
