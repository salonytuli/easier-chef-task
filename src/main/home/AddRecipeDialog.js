import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Typography from "@material-ui/core/Typography";
import { withFormik } from "formik";
import Box from "@material-ui/core/Box";
import * as Yup from "yup";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { addRecipe } from "./store/action/homeAction";
import AddIcon from "@material-ui/icons/Add";
import DeleteOutlineIcon from "@material-ui/icons/DeleteOutline";

const useStyles = makeStyles((theme) => ({
  ingredient: {
    padding: theme.spacing(2),
    marginTop: "10px",
  },
  delete: {
    position: "absolute",
    [theme.breakpoints.down("xs")]: {
      marginTop: "-150px",
      marginLeft: "-80px",
    },
    [theme.breakpoints.up("sm")]: {
      marginTop: "-100px",
      marginLeft: "-80px",
    },
  },
  icon: {
    cursor: "pointer",
    "&:hover": {
      background: "#bfbcbb",
    },
  },
}));
function DialogComponent(props) {
  const {
    toggleDialog,
    toggleDialogBox,
    handleChange,
    handleBlur,
    touched,
    errors,
    values,
    handleSubmit,
    setFieldValue,
  } = props;
  const handleIngredientChange = (event, index) => {
    const { name, value } = event.target;
    let data = values.ingredient;
    data[index] = { ...data[index], [name]: value };
    setFieldValue("ingredient", data);
  };
  const handleAddIngredient = () => {
    //method to add ingredient
    let data = values.ingredient;
    data = [...data, { name: "", quantity: "", unit: "" }];
    setFieldValue("ingredient", data);
  };
  const handleDelete = (index) => {
    //method to delete the ingredient detail box
    var data = values.ingredient;
    data.splice(index, 1);
    setFieldValue("ingredient", data);
  };
  const checkDisable = () => {
    //method to disable add button till ingredient name is not supplied
    let check = values.ingredient.filter((obj) => obj.name.trim() === "");
    if (check.length) {
      return true;
    } else {
      return false;
    }
  };
  const classes = useStyles();
  return (
    <div>
      <Dialog open={toggleDialog} fullWidth onClose={() => toggleDialogBox()}>
        <DialogTitle component="div" className="p-0">
          <Typography>Add Recipe</Typography>
        </DialogTitle>
        <DialogContent dividers style={{ position: "relative" }}>
          <TextField
            label="Name of the Dish"
            name="name"
            fullWidth
            value={values.name}
            onBlur={handleBlur}
            onChange={handleChange}
            error={!!touched.name && !!errors.name}
            helperText={touched.name && errors.name}
          />
          <TextField
            style={{ marginTop: 8 }}
            label="Steps"
            name="steps"
            variant="outlined"
            fullWidth
            multiline
            rows={3}
            value={values.steps}
            onBlur={handleBlur}
            onChange={handleChange}
            error={!!touched.steps && !!errors.steps}
            helperText={touched.steps && errors.steps}
          />
          <TextField
            label="Picture"
            name="pic"
            placeholder="Paste the url for the image"
            fullWidth
            value={values.pic}
            onBlur={handleBlur}
            onChange={handleChange}
            error={!!touched.pic && !!errors.pic}
            helperText={touched.pic && errors.pic}
          />
          <Grid
            container
            direction="column"
            justify="center"
            alignItems="flex-end"
            style={{ paddingTop: 10 }}
          >
            <Button
              variant="contained"
              color="secondary"
              className={classes.button}
              startIcon={<AddIcon />}
              size="small"
              onClick={() => {
                handleAddIngredient();
              }}
            >
              Add Ingredient
            </Button>
          </Grid>
          {values.ingredient.map((_, index) => (
            <Box
              key={index}
              border={1}
              className={classes.ingredient}
              borderRadius={16}
            >
              <Grid
                container
                direction="row"
                justify="space-between"
                alignItems="center"
              >
                {values.ingredient.length > 1 && (
                  <Grid
                    container
                    direction="row"
                    justify="flex-end"
                    alignItems="center"
                    className={classes.delete}
                  >
                    <DeleteOutlineIcon
                      color="secondary"
                      onClick={(_) => {
                        handleDelete(index);
                      }}
                      className={classes.icon}
                    />
                  </Grid>
                )}
                <TextField
                  label="Ingredient Name"
                  name="name"
                  value={values.ingredient[index]?.name}
                  fullWidth
                  onChange={(event) => {
                    handleIngredientChange(event, index);
                  }}
                />
                <TextField
                  label="Ingredient Quantity"
                  name="quantity"
                  type="number"
                  value={values.ingredient[index]?.quantity}
                  onChange={(event) => {
                    handleIngredientChange(event, index);
                  }}
                />
                <TextField
                  label="Quantity Unit"
                  name="unit"
                  value={values.ingredient[index]?.unit}
                  onChange={(event) => {
                    handleIngredientChange(event, index);
                  }}
                />
              </Grid>
            </Box>
          ))}
        </DialogContent>
        <DialogActions>
          <Button
            variant="contained"
            color="primary"
            size="small"
            disabled={checkDisable()}
            onClick={() => handleSubmit()}
          >
            Add
          </Button>
          <Button
            variant="contained"
            color="primary"
            size="small"
            onClick={() => toggleDialogBox()}
          >
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

const DialogComponentForm = withFormik({
  enableReinitialize: true,
  mapPropsToValues: () => ({
    name: "",
    steps: "",
    pic: "",
    ingredient: [
      {
        name: "",
        quantity: "",
        unit: "",
      },
    ],
  }),
  validationSchema: Yup.object().shape({
    name: Yup.string().required("Dish Name is required"),
    steps: Yup.string().required("Steps are Required"),
    pic: Yup.string().matches(
      /(https?:\/\/.*\.(?:png|jpg))/,
      "Enter correct url!"
    ),
  }),
  async handleSubmit(values, props) {
    props.props.addRecipe(values);
    props.props.toggleDialogBox();
  },
  displayName: "BasicForm",
})(DialogComponent);

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ addRecipe }, dispatch);
}
function mapStateToProps(state) {
  return {
    recipe: state.recipe,
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DialogComponentForm);
