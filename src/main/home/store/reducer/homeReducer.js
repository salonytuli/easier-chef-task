import * as Actions from "../action/homeAction";
let storageData = JSON.parse(localStorage.getItem("recipe"));

const initialState = {
  recipe: storageData ? storageData : [],
  filteredData: [],
};

const homeReducer = (state = initialState, action) => {
  switch (action.type) {
    case Actions.ADD_RECIPE: {
      return {
        ...state,
        recipe: state.recipe.concat(action.payload),
      };
    }
    case Actions.ADD_FILTERED_DATA: {
      return {
        ...state,
        filteredData: action.payload,
      };
    }
    default: {
      return state;
    }
  }
};

export default homeReducer;
