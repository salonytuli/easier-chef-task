export const ADD_RECIPE = "ADD_RECIPE";
export const ADD_FILTERED_DATA = "ADD_FILTERED_DATA";

export const addRecipe = (data) => {
  return (dispatch) => {
    let storageData = [];
    storageData = JSON.parse(localStorage.getItem("recipe"));
    if (storageData) storageData = [...storageData, data];
    else storageData = [data];
    localStorage.setItem("recipe", JSON.stringify(storageData));
    dispatch({
      type: ADD_RECIPE,
      payload: data,
    });
  };
};

export const addFilteredData = (data) => {
  return (dispatch) => {
    dispatch({
      type: ADD_FILTERED_DATA,
      payload: data,
    });
  };
};
