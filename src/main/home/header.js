import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Menu from "@material-ui/core/Menu";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Divider from "@material-ui/core/Divider";
import FilterListIcon from "@material-ui/icons/FilterList";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { addFilteredData } from "./store/action/homeAction";
import Hidden from "@material-ui/core/Hidden";
import AddIcon from "@material-ui/icons/Add";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  title: {
    flexGrow: 0,
    width: "220px",
  },
  button: {
    marginRight: "10px",
    marginBottom: "4px",
  },
}));

function Header(props) {
  const classes = useStyles();
  const { toggleDialogBox, recipeList, addFilteredData } = props;
  const [filter, setFilter] = useState({
    anchorEl: null,
    filteredArray: [],
  });
  const handleClick = (event) => {
    setFilter((prevState) => ({
      ...prevState,
      anchorEl: event.currentTarget,
    }));
  };
  const handleClose = () => {
    setFilter((prevState) => ({
      ...prevState,
      anchorEl: null,
    }));
  };
  const filterArray = () => {
    //method to find unique filter list
    let uniqueList = [];
    recipeList.map((obj) => {
      obj.ingredient.map((item) => {
        if (!uniqueList.includes(item.name.toLowerCase())) {
          uniqueList.push(item.name.toLowerCase());
        }
      });
    });
    return uniqueList;
  };
  const handleFilter = (event) => {
    //method to set the filtered list to redux
    let data = [];
    if (event.target.checked) {
      data = [...filter.filteredArray, event.target.name];
      setFilter((prevState) => ({
        ...prevState,
        filteredArray: data,
      }));
      addFilteredData(data);
    } else {
      data = filter.filteredArray.filter((obj) => obj !== event.target.name);
      setFilter((prevState) => ({
        ...prevState,
        filteredArray: data,
      }));
      addFilteredData(data);
    }
  };
  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            Recipe List
          </Typography>
          <Grid
            container
            direction="row"
            justify="flex-end"
            alignItems="center"
          >
            <Hidden only={["xs"]}>
              <Button
                className={classes.button}
                color="secondary"
                variant="contained"
                onClick={() => toggleDialogBox()}
                startIcon={<AddIcon />}
              >
                Add Recipes
              </Button>
            </Hidden>
            <Hidden only={["sm", "md", "lg", "xl"]}>
              <div>
                <Button
                  className={classes.button}
                  color="secondary"
                  variant="contained"
                  onClick={() => toggleDialogBox()}
                >
                  <AddIcon />
                </Button>
              </div>
            </Hidden>
            <Hidden only={["xs"]}>
              <Button
                variant="contained"
                color="secondary"
                className={classes.button}
                startIcon={<FilterListIcon />}
                onClick={(event) => handleClick(event)}
              >
                Filter
              </Button>
            </Hidden>
            <Hidden only={["sm", "md", "lg", "xl"]}>
              <div>
                <Button
                  variant="contained"
                  color="secondary"
                  className={classes.button}
                  onClick={(event) => handleClick(event)}
                >
                  <FilterListIcon />
                </Button>
              </div>
            </Hidden>
          </Grid>
        </Toolbar>
      </AppBar>
      <Menu
        anchorEl={filter.anchorEl}
        style={{ maxHeight: 400, maxWidth: 200 }}
        open={Boolean(filter.anchorEl)}
        onClose={handleClose}
        getContentAnchorEl={null}
        anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
        transformOrigin={{ vertical: "top", horizontal: "center" }}
      >
        {filterArray().map((obj, index) => (
          <div key={index} style={{ marginLeft: 10 }}>
            <FormControlLabel
              control={
                <Checkbox
                  onChange={(event) => handleFilter(event, index)}
                  name={obj}
                  color="primary"
                />
              }
              label={obj.toUpperCase()}
            />
            <Divider />
          </div>
        ))}
      </Menu>
    </div>
  );
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ addFilteredData }, dispatch);
}
export default connect(null, mapDispatchToProps)(Header);
