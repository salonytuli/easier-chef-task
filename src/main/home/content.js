import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import { Typography } from "@material-ui/core";
import AddRecipe from "./AddRecipeDialog";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: "10px",
    width: "95%",
    padding: "20px",
    overflow: "auto",
  },
  button: {
    marginRight: "20px",
    marginBottom: "4px",
  },
  media: {
    height: "90px",
    alignItems: "center",
    textAlign: "center",
    padding: theme.spacing(2),
  },
  card: {
    width: 300,
    marginBottom: 20,
  },
  content: {
    height: 40,
    overflow: "auto",
  },
}));

export default function Content(props) {
  const classes = useStyles();
  const { state, toggleDialogBox, recipeList } = props;

  return (
    <div
      className={classes.root}
      style={{
        height: "100%",
        overflow: "auto",
      }}
    >
      {
        <Grid
          container
          direction="row"
          justify="space-evenly"
          alignItems="flex-start"
        >
          {recipeList.length ? (
            recipeList.map((item, index) => (
              <Card key={index} className={classes.card}>
                <CardHeader title={item.name} />
                <CardMedia
                  className={classes.media}
                  image={item.pic !== "" ? item.pic : "/unavailable-image.jpg"}
                  title="Paella dish"
                />
                <CardContent className={classes.content}>
                  {item.ingredient.map((obj, index) => (
                    <Typography
                      key={index}
                      noWrap
                      variant="body2"
                      color="textSecondary"
                      component="p"
                    >
                      {obj.name}
                    </Typography>
                  ))}
                </CardContent>
              </Card>
            ))
          ) : (
            <div>
              <Paper className={classes.paper}>
                <Typography noWrap>{"NO DATA TO SHOW"}</Typography>
              </Paper>
            </div>
          )}
        </Grid>
      }
      {state.toggleDialog && (
        <AddRecipe
          toggleDialog={state.toggleDialog}
          toggleDialogBox={toggleDialogBox}
        />
      )}
    </div>
  );
}
