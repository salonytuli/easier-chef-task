import React, { useState } from "react";
import Header from "./header";
import Content from "./content";
import { connect } from "react-redux";

function Home(props) {
  const [state, setState] = useState({
    toggleDialog: false,
  });
  const { recipe, filterData } = props;
  const toggleDialogBox = () => {
    setState((prevState) => ({
      ...prevState,
      toggleDialog: !prevState.toggleDialog,
    }));
  };
  const filterArray = () => {
    let uniqueList = [];
    if (filterData.length) {
      recipe.map((item) => {
        let temp = item.ingredient.filter((obj) =>
          filterData.includes(obj.name.toLowerCase())
        );
        if (temp.length) {
          uniqueList.push(item);
        }
      });
      return uniqueList;
    } else {
      return recipe;
    }
  };
  return (
    <div
      style={{
        height: "620px",
        overflow: "hidden",
      }}
    >
      <Header toggleDialogBox={toggleDialogBox} recipeList={recipe} />
      <Content
        state={state}
        toggleDialogBox={toggleDialogBox}
        recipeList={filterArray()}
      />
    </div>
  );
}
function mapStateToProps(state) {
  return {
    recipe: state.home.recipe,
    filterData: state.home.filteredData,
  };
}
export default connect(mapStateToProps, null)(Home);
